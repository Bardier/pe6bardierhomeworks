// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Это функция которая срабатывает при каком-то определенном событии, а именно клик, или наведение мышки,
// нажатие клавиши и тд...

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.

`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const container = document.querySelector(`.container`);
	const inputPrice = document.createElement(`input`);
	const priceOut = document.createElement(`span`);
	const wrongPriceOut = document.createElement(`span`);

	inputPrice.setAttribute(`type`, `text`);
	container.textContent = `Price: `;
	container.append(inputPrice);

	inputPrice.addEventListener(`focus`, inputPriceOnFocus);
	inputPrice.addEventListener(`blur`, inputPriceOnBlur);

	function inputPriceOnFocus() {
		this.classList.add(`border-green`);
		this.classList.remove(`color-green`);
		this.classList.remove(`color-red`);
		this.value = ``;

		[...container.children].forEach(element => {
			if (element === priceOut) {
				container.removeChild(priceOut);
			}
			if (element === wrongPriceOut) {
				container.removeChild(wrongPriceOut);
			}
		});

	}
	function inputPriceOnBlur() {
		this.classList.remove(`border-green`);

		if (!isNaN(+inputPrice.value) && inputPrice.value.trim()) {
			if (inputPrice.value < 0) {
				this.classList.add(`color-red`);
				this.classList.remove(`color-green`);
				creatinWrongPrice();
				return;
			}
			this.classList.add(`color-green`);
			this.classList.remove(`color-red`);
			creatingPrice();
		}
	}

	function creatingPrice() {
		priceOut.classList.add(`price-out`);
		priceOut.textContent = `Текущая цена: ${inputPrice.value}`;

		const priceRemove = document.createElement(`span`);
		priceRemove.textContent = `X`;
		priceRemove.classList.add(`price-remove`);

		priceOut.append(priceRemove);
		container.prepend(priceOut);

		priceRemove.addEventListener(`click`, priceRemoveClick);
	}

	function priceRemoveClick(event) {
		container.removeChild(event.target.parentNode);
		inputPrice.value = ``;
		inputPrice.focus();
	};

	function creatinWrongPrice() {
		wrongPriceOut.textContent = `Please enter correct price.`;
		wrongPriceOut.classList.add(`price-out-wrong`);
		wrongPriceOut.classList.add(`color-red`);

		container.prepend(wrongPriceOut);
	};

});

