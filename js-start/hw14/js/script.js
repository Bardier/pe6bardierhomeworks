`use strict`;

$(document).ready(function () {

	// ! плавная прокрутка
	$(document).on(`click`, ".nav__link", function (e) {
		e.preventDefault();
		$(`html`).animate({
			scrollTop: $($(this).attr('href')).offset().top - $(`.nav`).height(),
		}, 500);
	});

	// ! кнопка наверх
	const btnTop = document.createElement(`button`);
	$(btnTop).text(`Наверх`);
	$(btnTop).css({
		position: `fixed`,
		"background-color": `red`,
		bottom: `10px`,
		right: `20px`,
		padding: `10px 15px`,
		cursor: `pointer`,
	});
	$(`body`).append(btnTop);

	if ($(window).scrollTop() >= $(window).height()) {
		$(btnTop).css(`display`, `block`);
	} else $(btnTop).css(`display`, `none`);

	$(window).scroll(function () {
		if ($(this).scrollTop() >= $(window).height()) {
			$(btnTop).css(`display`, `block`);
		} else $(btnTop).css(`display`, `none`);
	});

	$(btnTop).click(function (e) {
		e.preventDefault();
		$(`html`).animate({
			scrollTop: 0,
		}, 500);
	});

	// ! hide section
	const btnHide = document.createElement(`button`);
	$(btnHide).text(`Hide section`);
	$(btnHide).css({
		padding: `10px 15px`,
		background: `greey`,
		border: `2px solid #000000`,
		"border-radius": `3px`,
		cursor: `pointer`
	});

	$(`.popular`).append(btnHide);

	$(btnHide).click(function (e) {
		e.preventDefault();

		if ($(btnHide).text() === `Hide section`) {
			$(btnHide).text(`Show section`);
		} else $(btnHide).text(`Hide section`);

		$(`.popular__posts`).slideToggle(`slow`);
	});

});

