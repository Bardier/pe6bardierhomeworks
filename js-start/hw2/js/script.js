// Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
// Цыклы в первую очередь нужны, дабы выполнить одинаковые действия какое-то определенное количество раз или пока не будет выполненно какое-то условие.

'use strict';

let number;

do {
	number = prompt(`Enter number.`);
} while (
	!number
	|| isNaN(+number)
);

for (let i = 0; i <= number; i++) {
	if (i % 5 === 0) {
		console.log(i);
	}
}

// !Вторая задача

let smallNumber, bigNumber, flag = `Простое число!!!`;

do {
	smallNumber = prompt('Enter first number.');
	bigNumber = prompt('Enter second number.');
} while (
	!smallNumber
	|| isNaN(+smallNumber)
	|| !bigNumber
	|| isNaN(+bigNumber)
	|| +bigNumber < +smallNumber
	|| +smallNumber <= 1
);

smallNumber = +smallNumber;
bigNumber = +bigNumber;

for (smallNumber; smallNumber < bigNumber; smallNumber++) {
	if (bigNumber % smallNumber === 0) {
		flag = `Не простое число`;
		break;
	}
}
console.log(flag);