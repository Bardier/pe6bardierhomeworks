// 1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
// setTimeout() - функция которая сработате один раз через заданное время.
// setInterval() - будет срабатывать каждый раз через заданный интервал времени
// 2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Она сработает после выполнения текущего кода.
// 3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
// Чтоб очистить память.

`use strict`

document.addEventListener(`DOMContentLoaded`, () => {

	const sliderImgs = [...document.querySelectorAll(`.image-to-show`)];
	const btnStop = document.querySelector(`.btn-stop`);
	const btnStart = document.querySelector(`.btn-start`);
	let activeIndex = 0;
	let sliderTimer;

	function slider() {
		sliderTimer = setInterval(() => {

			sliderImgs.forEach((element, index) => {
				if (element.classList.contains(`active`)) {
					if (index === sliderImgs.length - 1) {
						activeIndex = 0
					} else {
						activeIndex = index + 1;
					}
				}
				element.classList.remove(`active`);
			});


			sliderImgs[activeIndex].classList.add(`active`);
		}, 3000);
	}

	slider();

	btnStop.addEventListener(`click`, () => {
		clearInterval(sliderTimer);
	})
	btnStart.addEventListener(`click`, () => {
		clearInterval(sliderTimer);
		slider();
	})

});