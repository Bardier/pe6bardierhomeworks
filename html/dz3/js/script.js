`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const allElements = [
		document.querySelector(`.body`),
		document.querySelector(`.page__header`),
		document.querySelector(`.page__main`),
		document.querySelector(`.page__footer`),
	];
	const btn = document.querySelector(`.theme-color`);

	changingThemeColor();

	btn.addEventListener(`click`, btnOnCLick);

	function btnOnCLick() {
		if (btn.textContent === `Темная тема`) {
			localStorage.setItem(`themeColor`, `dark`);
			changingThemeColor();
			btn.textContent = `Светлая тема`;
		} else if (btn.textContent === `Светлая тема`) {
			localStorage.setItem(`themeColor`, `light`);
			changingThemeColor();
			btn.textContent = `Темная тема`;
		}
	}

	function changingThemeColor() {
		const themeColor = localStorage.getItem(`themeColor`);
		if (!themeColor || themeColor === `light`) {
			allElements.forEach(element => {
				element.classList.remove(`bg-dark`);
				element.classList.add(`bg-light`);
			});
		} else if (themeColor === `dark`) {
			allElements.forEach(element => {
				element.classList.remove(`bg-light`);
				element.classList.add(`bg-dark`);
			});
		}
	}


});