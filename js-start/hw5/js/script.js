// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Экранирование используем для того чтоб показать интерпретатору что следующий символ по коду будет 
// просто символом, а не частью выражения. Пример: экранирование кавычек.
// Также бывает и наоборот когда нужно указать интерпретатору что будет использоваться часть выражения,
// а не строка. Пример: добавление символа из Unicode \u{1f600} 😀.

'use strict';

function createNewUser() {
	const newUser = {
		_setFirstName: ``,
		set setFirstName(value) {
			this._setFirstName = value;
		},
		get setFirstName() {
			return this._setFirstName;
		},

		_setLastName: ``,
		set setLastName(value) {
			this._setLastName = value;
		},
		get setLastName() {
			return this._setLastName;
		},

		_birthday: new Date(),
		set birthday(value) {
			this._birthday = value;
		},
		get birthday() {
			return this._birthday;
		},

		getLogin() {
			return `${this.setFirstName[0]}${this.setLastName}`.toLowerCase();
		},

		getAge() {
			return Math.floor((Date.parse(new Date()) - Date.parse(this.birthday)) / (1000 * 60 * 60 * 24 * 364.25));
		},

		getPassword() {
			return `${this.setFirstName[0].toUpperCase()}${this.setLastName.toLowerCase()}${this.birthday.getFullYear()}`;
		},
	};

	newUser.setFirstName = document.querySelector('#user-first-name').value;
	newUser.setLastName = document.querySelector('#user-last-name').value;
	newUser.birthday = new Date(document.querySelector('#user-birthday').value);

	console.log(newUser.getLogin());
	console.log(newUser.getAge());
	console.log(newUser.getPassword());

	return newUser;
};

document.querySelector('#user-create-btn').addEventListener('click', () => {
	console.log(createNewUser());
});
