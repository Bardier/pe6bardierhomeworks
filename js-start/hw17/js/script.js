// Создать пустой объект student, с полями name и lastName.
// Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
// Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
// Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.

`use strict`;

document.addEventListener(`DOMContentLoaded`, () => {

	const student = {
		_name: ``,
		get name() {
			return this._name;
		},
		set name(value) {
			this._name = value;
		},

		_lastName: ``,
		get lastName() {
			return this._lastName;
		},
		set lastName(value) {
			this._lastName = value;
		},

		tabel: {},

		_newCours: true,
		get newCours() {
			return this._newCours;
		},
		set newCours(value) {
			this._newCours = value;
		},

		_scholarship: 0,
		get scholarship() {
			return this._scholarship;
		},
		set scholarship(value) {
			this._scholarship = value;
		},

	};

	student.name = prompt(`Имя студента`);
	student.lastName = prompt(`Фамилия студента`);

	while (true) {
		const subject = prompt(`Название предмета`);
		if (!subject) break;
		const subjectGrade = +prompt(`Оценка по этому предмету`);

		student.tabel[subject] = subjectGrade;
	}

	for (const key in student.tabel) {
		if (student.tabel[key] < 4) {
			student.newCours = false;
		}
		student.scholarship += student.tabel[key];
	}

	if (student.newCours) {
		alert(`Студент переведен на следующий курс`);
	}

	if ((student.scholarship / Object.keys(student.tabel).length) > 7) {
		alert(`Студенту назначена стипендия.`);
	}

});