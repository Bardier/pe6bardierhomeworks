// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// var устаревший вариант, let и const современный который убирает целую кучу головной боли. 

// Почему объявлять переменную через var считается плохим тоном?
// 1. var проблема с зоной видимости, а именно видна всегда и везде (нет понятия зоны видимости). 
// 2. Можно спокойно перезаписать в отличии от const.
// 3. Использовать можно до ее обьявления по коду. 
// 4. Также засоряет глобальный обьект window. 

'use strict';

let userName, userAge;

do {
	userName = prompt(`Enter your name.`);
	userAge = prompt(`Enter your age.`);
} while (
	!userName
	|| !userAge
	|| isNaN(+userAge)
);

if (userAge < 18) {
	alert(`You are not allowed to visit this website`)
}
else if (userAge >= 18 && userAge <= 22) {
	const userAgeConfirm = confirm(`Are you sure you want to continue?`);
	if (userAgeConfirm) {
		alert(`Welcome, ${userName}.`);
	}
	else {
		alert(`You are not allowed to visit this website.`);
	}
}
else if (userAge > 22) {
	alert(`Welcome, ${userName}.`);
}