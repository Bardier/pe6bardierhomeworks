document.addEventListener('DOMContentLoaded', () => {
	document.addEventListener('scroll', () => {
		if (window.scrollY >= 20) {
			document.querySelector('.page-header').style.position = 'fixed';
			document.querySelector('.page-header').style.top = '0';
		}
		if(window.scrollY < 20){
			document.querySelector('.page-header').style.position = 'absolute';
			document.querySelector('.page-header').style.top = `20px`;
		}
	})
});